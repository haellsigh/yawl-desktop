#include "qgenrelabel.h"

QGenreLabel::QGenreLabel(QString &genre)
{
    setStyleSheet("background-color: #3676A3; color: #FFF; font-family: 'Lucida Grande', Tahoma, Arial, sans-serif, sans; font-size: 11px; padding-bottom: 1px; padding-left: 5px; padding-right: 5px; padding-top: 1px; border: 1px solid #FFF;");
    setText(genre);
    setFixedSize(minimumSizeHint());
}
