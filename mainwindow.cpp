#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->setContentsMargins(2, 2, 5, 2);
    m_progressBar = new QProgressBar(this);
    m_progressBar->setRange(0, 100);
    m_progressBar->setValue(0);
    m_progressBar->setTextVisible(false);
    ui->statusBar->addPermanentWidget(m_progressBar);
    ui->actionExit->setStatusTip("Exit the application.");
    QUrl url;
    url.setUrl("http://yawl-c9-haellsigh.c9.io/animes");

    downloader = new QRestService(url);
    downloader->GET();
    connect(downloader, SIGNAL(downloadFinished(QByteArray)), SLOT(setMainText(QByteArray)));

    connect(ui->actionAddAnime, SIGNAL(triggered()), SLOT(addAnime()));
    connect(ui->actionRefreshAnimes, SIGNAL(triggered()), SLOT(refreshAnimes()));
    connect(ui->listWidget, SIGNAL(addAnime()), SLOT(addAnime()));

    ui->statusBar->showMessage("Loading data...");
}

MainWindow::~MainWindow()
{
    exit(0);
    delete ui;
}

void MainWindow::setMainText(QByteArray text)
{
    ui->statusBar->showMessage("Finished loading.");
    m_progressBar->setValue(100);
    QJsonDocument document;
    document = QJsonDocument::fromJson(text);

    QStringList myData;
    QJsonArray usersArray = document.object().value("_items").toArray();
    for(int i = 0; i < usersArray.size(); i++)
    {
        myData.append(usersArray.at(i).toObject().value("title").toString("lolfail"));
    }

    ui->listWidget->addItems(myData);
}

void MainWindow::addAnime()
{
    createAnime = new FormCreateAnime();
    createAnime->setWindowModality(Qt::ApplicationModal);
    createAnime->show();
}

void MainWindow::refreshAnimes()
{
    ui->statusBar->showMessage("Loading data...");
    m_progressBar->setValue(0);
    ui->listWidget->clear();
    downloader->GET();
}
