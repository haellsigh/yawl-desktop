#ifndef QANIMELISTWIDGET_H
#define QANIMELISTWIDGET_H

#include <QListWidget>
#include <QContextMenuEvent>
#include <QMenu>
#include "qanimedisplay.h"

#include "qrestservice.h"

class QAnimeListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit QAnimeListWidget(QWidget *parent = 0);

protected:
    void contextMenuEvent(QContextMenuEvent *event);

signals:
    void addAnime();

public slots:
    void openCustomMenu(const QPoint &pos);
    void deleteSelectedAnimes();

private:
    QMenu *m_menu;
    QList<QAction *> m_animeActions;
    QRestService *deleteService;

};

#endif // QANIMELISTWIDGET_H
