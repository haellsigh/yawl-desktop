#ifndef FORMCREATEANIME_H
#define FORMCREATEANIME_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTextCursor>
#include <QLabel>

#include "qrestservice.h"

namespace Ui {
class FormCreateAnime;
}

class FormCreateAnime : public QWidget
{
    Q_OBJECT

public:
    explicit FormCreateAnime(QWidget *parent = 0);
    ~FormCreateAnime();

    QByteArray composeJson();

public slots:
    void updateIdentifier(QString title);
    void addAnime();

private slots:
    void on_genresEdit_clicked();

    void on_bAddAnime_clicked();

private:
    Ui::FormCreateAnime *ui;
    QLabel *lolWhat;

    QNetworkAccessManager *m_manager;
};

#endif // FORMCREATEANIME_H
