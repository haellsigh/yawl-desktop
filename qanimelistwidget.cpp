#include "qanimelistwidget.h"

#include <QDebug>

QAnimeListWidget::QAnimeListWidget(QWidget *parent) :
    QListWidget(parent)
{
    setSelectionMode(QAbstractItemView::ExtendedSelection);

    m_animeActions.append(new QAction("&Add anime", this));
    m_animeActions.append(new QAction("&Show anime", this));
    m_animeActions.append(new QAction("&Delete anime(s)", this));

    connect(m_animeActions.at(0), SIGNAL(triggered()), SIGNAL(addAnime()));
    connect(m_animeActions.at(2), SIGNAL(triggered()), SLOT(deleteSelectedAnimes()));
}

void QAnimeListWidget::openCustomMenu(const QPoint &pos)
{
    QAnimeDisplay *animeDisplay = new QAnimeDisplay(this);

    animeDisplay->show();
}

void QAnimeListWidget::contextMenuEvent(QContextMenuEvent *event)
{
    event->accept();
    QMenu *menu = new QMenu("Anime menu");
    menu->addAction(m_animeActions.at(0));
    menu->addSeparator();
    menu->addActions(m_animeActions.mid(1));

    qDebug() << itemAt(event->pos());

    if(itemAt(event->pos()) == NULL)
    {
        menu->actions().at(2)->setEnabled(false);
        menu->actions().at(3)->setEnabled(false);
    }
    else
    {
        menu->actions().at(2)->setEnabled(true);
        menu->actions().at(3)->setEnabled(true);
    }

    menu->exec(event->globalPos());
    menu->deleteLater();
}

void QAnimeListWidget::deleteSelectedAnimes()
{
    foreach (QListWidgetItem *item, selectedItems()) {
        //deleteService = new QRestService()
        //no access to the item ID here, can't delete from here!
        qDebug() << "Removing item: " << item->text();
        delete item;
    }

}
