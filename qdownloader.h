#ifndef QDOWNLOADER_H
#define QDOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

class QDownloader : public QObject
{
    Q_OBJECT
public:
    explicit QDownloader(QUrl &url);

signals:
    void downloadFinished(QByteArray data);

public slots:
    void replyFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager *m_manager;

};

#endif // QDOWNLOADER_H
