#ifndef QGENREPICKERWINDOW_H
#define QGENREPICKERWINDOW_H

#include <QWidget>
#include <QCheckBox>
#include <QJsonDocument>
#include <QJsonArray>
#include <QPushButton>

namespace Ui {
class QGenrePickerWindow;
}

class QGenrePickerWindow : public QWidget
{
    Q_OBJECT

public:
    explicit QGenrePickerWindow(QWidget * parent = 0, Qt::WindowFlags f = 0);
    ~QGenrePickerWindow();

    QString getJson() { return mJson; };

signals:
    void readyJson();

public slots:
    void generateJson();

private:
    Ui::QGenrePickerWindow *ui;

    QPushButton *bSave;
    QString mJson;
    QVector<QCheckBox*> checkboxes;
};

#endif // QGENREPICKERWINDOW_H
