#include "formcreateanime.h"
#include "ui_formcreateanime.h"

#include <QDebug>

FormCreateAnime::FormCreateAnime(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormCreateAnime)
{
    ui->setupUi(this);

    connect(ui->titleEdit, SIGNAL(textChanged(QString)), SLOT(updateIdentifier(QString)));

    connect(ui->bAddAnime, SIGNAL(clicked()), SLOT(addAnime()));
}

FormCreateAnime::~FormCreateAnime()
{
    delete m_manager;
    delete ui;
}

void FormCreateAnime::updateIdentifier(QString title)
{
    ui->identifierEdit->setText(title.remove(QRegularExpression("\\W")).simplified().toLower());
}

void FormCreateAnime::addAnime()
{
    lolWhat = new QLabel(composeJson());
    lolWhat->setWindowModality(Qt::ApplicationModal);
    lolWhat->show();

    m_manager = new QNetworkAccessManager(this);
    //connect(m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(replyFinished(QNetworkReply*)));

    QNetworkRequest request;
    request.setUrl(QUrl("http://yawl-c9-haellsigh.c9.io/animes"));
    request.setRawHeader("Content-Type", "application/json");

    m_manager->post(request, composeJson());
}

QByteArray FormCreateAnime::composeJson()
{
    QJsonDocument doc;
    QJsonObject object;

    object.insert("identifier", ui->identifierEdit->text());
    object.insert("title", ui->titleEdit->text());
    object.insert("synopsis", ui->synopsisEdit->document()->toPlainText().remove("\""));
    QJsonArray array = QJsonDocument::fromJson(ui->genresEdit->getJson().toUtf8()).array();
    object.insert("genres", array);
    object.insert("status", ui->statusEdit->currentText());
    object.insert("episodes", ui->episodesEdit->text().toInt());

    doc.setObject(object);

    return doc.toJson();



    /*QByteArray jsonBA;
    jsonBA.clear();

    jsonBA.append("{\"identifier\":\"");
    jsonBA.append(ui->identifierEdit->text());
    jsonBA.append("\",\"title\":\"");
    jsonBA.append(ui->titleEdit->text());
    jsonBA.append("\",\"synopsis\":\"");
    jsonBA.append(ui->synopsisEdit->document()->toPlainText().remove("\""));
    jsonBA.append("\",\"genres\":");
    jsonBA.append(ui->genresEdit->getJson());
    jsonBA.append(",\"status\":\"");
    jsonBA.append(ui->statusEdit->currentText());
    jsonBA.append("\",\"episodes\":");
    jsonBA.append(ui->episodesEdit->text());
    jsonBA.append("}");

    return jsonBA;*/
}

void FormCreateAnime::on_genresEdit_clicked()
{
    ui->genresEdit->openWindow();
}

void FormCreateAnime::on_bAddAnime_clicked()
{

}
