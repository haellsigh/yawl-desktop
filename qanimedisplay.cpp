#include "qanimedisplay.h"
#include "ui_qanimedisplay.h"

QAnimeDisplay::QAnimeDisplay(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::QAnimeDisplay)
{
    ui->setupUi(this);
}

QAnimeDisplay::~QAnimeDisplay()
{
    delete ui;
}
