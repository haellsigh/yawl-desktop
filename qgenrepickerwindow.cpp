#include "qgenrepickerwindow.h"
#include "ui_qgenrepickerwindow.h"

QGenrePickerWindow::QGenrePickerWindow(QWidget *parent, Qt::WindowFlags f) :
    QWidget(parent),
    ui(new Ui::QGenrePickerWindow)
{
    ui->setupUi(this);
    ui->gridLayout_2->setColumnStretch(4, 1);

    bSave = new QPushButton("Save");

    QJsonDocument doc = QJsonDocument::fromJson("[\"Action\", \"Adventure\", \"Cars\", \"Comedy\", \"Dementia\", \"Demons\", \"Drama\", \"Ecchi\", \"Fantasy\", \"Game\", \"Harem\", \"Hentai\", \"Historical\", \"Horror\", \"Josei\", \"Kids\", \"Magic\", \"Martial Arts\", \"Mecha\", \"Military\", \"Music\", \"Mystery\", \"Parody\", \"Police\", \"Psychological\", \"Romance\", \"Samurai\", \"School\", \"Sci-Fi\", \"Seinen\", \"Shoujo\", \"Shoujo Ai\", \"Shounen\", \"Shounen Ai\", \"Slice of Life\", \"Space\", \"Sports\", \"Super Power\", \"Supernatural\", \"Thriller\", \"Vampire\", \"Yaoi\", \"Yuri\"]");
    QJsonArray array = doc.array();

    for(int i = 0; i < array.size(); i++) {
        checkboxes.insert(i, new QCheckBox(array.at(i).toString("ERROR")));
        ui->gridLayout_2->addWidget(checkboxes.at(i));
    }

    ui->gridLayout_2->addWidget(bSave, 8, 3, 1, 2);

    connect(bSave, SIGNAL(clicked()), SLOT(generateJson()));
}

QGenrePickerWindow::~QGenrePickerWindow()
{
    delete ui;
}

void QGenrePickerWindow::generateJson()
{
    QJsonDocument doc;
    QJsonArray array;

    for(int i = 0; i < checkboxes.count(); i++)
        if(checkboxes.at(i)->isChecked())
            array.append(QJsonValue(checkboxes.at(i)->text()));

    doc.setArray(array);
    mJson = QString(doc.toJson());

    emit readyJson();
    close();
}
