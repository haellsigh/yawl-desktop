#include "qdownloader.h"

QDownloader::QDownloader(QUrl &url)
{
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(replyFinished(QNetworkReply*)));

    m_manager->get(QNetworkRequest(url));
}

void QDownloader::replyFinished(QNetworkReply *reply)
{
    emit downloadFinished(reply->readAll());
    reply->deleteLater();
}
