#ifndef QANIMEDISPLAY_H
#define QANIMEDISPLAY_H

#include <QFrame>

namespace Ui {
class QAnimeDisplay;
}

class QAnimeDisplay : public QFrame
{
    Q_OBJECT

public:
    explicit QAnimeDisplay(QWidget *parent = 0);
    ~QAnimeDisplay();

private:
    Ui::QAnimeDisplay *ui;
};

#endif // QANIMEDISPLAY_H
