#include "qrestservice.h"

QRestService::QRestService(QUrl &url) :
    m_url(url)
{
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(replyFinished(QNetworkReply*)));
}

void QRestService::replyFinished(QNetworkReply *reply)
{
    emit downloadFinished(reply->readAll());
    reply->deleteLater();
}

void QRestService::GET()
{
    m_manager->get(QNetworkRequest(m_url));
}

void QRestService::POST(QByteArray data)
{
    QNetworkRequest request;
    request.setUrl(m_url);
    request.setRawHeader("Content-Type", "application/json");

    m_manager->post(request, data);
}

void QRestService::DELETE()
{
    QNetworkRequest request;
    request.setUrl(m_url);
    request.setRawHeader("Content-Type", "application/json");

    m_manager->deleteResource(request);
}
