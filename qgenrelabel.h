#ifndef QGENRELABEL_H
#define QGENRELABEL_H

#include <QLabel>

class QGenreLabel : public QLabel
{
    Q_OBJECT
public:
    explicit QGenreLabel(QString &genre);

signals:

public slots:

};

#endif // QGENRELABEL_H
