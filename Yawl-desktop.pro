#-------------------------------------------------
#
# Project created by QtCreator 2014-07-27T21:59:32
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Yawl-desktop
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qdownloader.cpp \
    qjsonview.cpp \
    formcreateanime.cpp \
    qgenrelabel.cpp \
    qrestservice.cpp \
    qanimedisplay.cpp \
    qanimelistwidget.cpp \
    qgenrepicker.cpp \
    qgenrepickerwindow.cpp

HEADERS  += mainwindow.h \
    qdownloader.h \
    qjsonview.h \
    formcreateanime.h \
    qgenrelabel.h \
    qrestservice.h \
    qanimedisplay.h \
    qanimelistwidget.h \
    qgenrepicker.h \
    qgenrepickerwindow.h

FORMS    += mainwindow.ui \
    formcreateanime.ui \
    qanimedisplay.ui \
    qgenrepickerwindow.ui
