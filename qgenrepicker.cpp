#include "qgenrepicker.h"

#include <QDebug>

QGenrePicker::QGenrePicker(QWidget *parent)
{
    pickerWindow = new QGenrePickerWindow();
    connect(pickerWindow, SIGNAL(readyJson()), SLOT(updateJson()));
}

void QGenrePicker::openWindow()
{
    qDebug() << "Opening genrePicker window...";
    pickerWindow->setWindowModality(Qt::ApplicationModal);
    pickerWindow->show();
}

void QGenrePicker::updateJson()
{
    mJson = pickerWindow->getJson();
    qDebug() << mJson;
}
