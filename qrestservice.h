#ifndef QRESTSERVICE_H
#define QRESTSERVICE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

class QRestService : public QObject
{
    Q_OBJECT
public:
    explicit QRestService(QUrl &url);

signals:
    void downloadFinished(QByteArray data);

public slots:
    void GET();
    void POST(QByteArray data);
    void DELETE();

    void replyFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager *m_manager;
    QUrl m_url;

};

#endif // QRESTSERVICE_H
