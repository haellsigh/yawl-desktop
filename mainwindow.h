#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressBar>

#include "qrestservice.h"
#include "formcreateanime.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void setMainText(QByteArray text);
    void addAnime();
    void refreshAnimes();

private:
    Ui::MainWindow *ui;
    QProgressBar *m_progressBar;

    FormCreateAnime *createAnime;

    QRestService *downloader;
};

#endif // MAINWINDOW_H
