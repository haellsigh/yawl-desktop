#ifndef QGENREPICKER_H
#define QGENREPICKER_H

#include <QPushButton>

#include "qgenrepickerwindow.h"

class QGenrePicker : public QPushButton
{
    Q_OBJECT
public:
    explicit QGenrePicker(QWidget *parent = 0);

    void openWindow();

    QString getJson() { return mJson; };

signals:

public slots:
    void updateJson();
private:
    QGenrePickerWindow *pickerWindow;
    QString mJson;

};

#endif // QGENREPICKER_H
